#define FUSE_USE_VERSION 31
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <stdbool.h>
#include <dirent.h>

#define MAX_LINE_LENGTH 256
#define CREDENTIALS_FILE "credentials.txt"
#define SECRET_FOLDER_PATH "/home/kali/Documents/Sisop/shift-4/soal5/rahasia"
#define MOUNTPOINT_PATH "/home/kali/Documents/Sisop/shift-4/soal5/mountpoint"
#define RESULT_FILE_PATH "/home/kali/Documents/Sisop/shift-4/soal5/result.txt"
#define EXTENSION_FILE_PATH "/home/kali/Documents/Sisop/shift-4/soal5/extension.txt"

bool is_authenticated = false;

typedef struct {
    char username[MAX_LINE_LENGTH];
    char password[MAX_LINE_LENGTH];
} User;

int fileExists(const char* file) {
    return access(file, F_OK) != -1;
}

int directoryExists(const char* directory) {
    struct stat st;
    if (stat(directory, &st) == -1) {
        return 0;
    }
    return S_ISDIR(st.st_mode);
}

int downloadFile(const char* url, const char* outputFileName) {
    if (fileExists(outputFileName)) {
        printf("File %s sudah ada, tidak perlu mendownload lagi\n", outputFileName);
        return 0;
    }
    
    char command[256];
    sprintf(command, "wget -O %s \"%s\"", outputFileName, url);
    
    int result = system(command);
    
    if (result != 0) {
        fprintf(stderr, "Gagal mendownload file zip\n");
        return 1;
    }
    
    return 0;
}

int unzipFile(const char* zipFileName) {
    if (directoryExists(SECRET_FOLDER_PATH)) {
        printf("Direktori rahasia sudah ada, tidak perlu melakukan unzip lagi\n");
        return 0;
    }

    char parent_directory[MAX_LINE_LENGTH];
    strcpy(parent_directory, SECRET_FOLDER_PATH);
    char *last_slash = strrchr(parent_directory, '/');
    if (last_slash != NULL) {
        *last_slash = '\0';
    }

    char command[256];
    sprintf(command, "unzip -o %s -d %s", zipFileName, parent_directory);
    
    int result = system(command);
    
    if (result != 0) {
        fprintf(stderr, "Gagal melakukan unzip\n");
        return 1;
    }
    
    return 0;
}

void md5hash(const char* input, char* output) {
    char command[MAX_LINE_LENGTH];
    sprintf(command, "echo -n %s | md5sum | awk '{ print $1 }'", input);

    FILE* stream = popen(command, "r");
    fgets(output, MAX_LINE_LENGTH, stream);
    output[strlen(output) - 1] = '\0'; // Remove newline at the end
    pclose(stream);
}

void registerUser(const User* user) {
    FILE* file = fopen(CREDENTIALS_FILE, "a+");
    if (!file) {
        fprintf(stderr, "Gagal membuka file kredensial\n");
        return;
    }

    char line[MAX_LINE_LENGTH];
    while (fgets(line, MAX_LINE_LENGTH, file)) {
        User existingUser;
        sscanf(line, "%[^;];%s\n", existingUser.username, existingUser.password);

        if (strcmp(user->username, existingUser.username) == 0) {
            fprintf(stderr, "Username sudah ada\n");
            fclose(file);
            return;
        }
    }

    fprintf(file, "%s;%s\n", user->username, user->password);
    fclose(file);
}

int loginUser(const User* user) {
    FILE* file = fopen(CREDENTIALS_FILE, "r");
    if (!file) {
        fprintf(stderr, "Gagal membuka file kredensial\n");
        return 1;
    }

    char line[MAX_LINE_LENGTH];
    while (fgets(line, MAX_LINE_LENGTH, file)) {
        User existingUser;
        sscanf(line, "%[^;];%s\n", existingUser.username, existingUser.password);

        if (strcmp(user->username, existingUser.username) == 0 && strcmp(user->password, existingUser.password) == 0) {
            fclose(file);
            return 0;
        }
    }

    fclose(file);
    return 1;
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char new_path[MAX_LINE_LENGTH];

    if (strcmp(path, "/") == 0) {
        sprintf(new_path, "%s", SECRET_FOLDER_PATH);
    } else {
        sprintf(new_path, "%s%s", SECRET_FOLDER_PATH, path);
    }

    res = lstat(new_path, stbuf);

    if (res == -1) {
        return -errno;
    }
    
    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    (void) offset;
    (void) fi;

    if (!is_authenticated) {
        printf("Please login first.\n");
        return -1;
    }

    char new_path[MAX_LINE_LENGTH];

    if (strcmp(path, "/") == 0) {
        sprintf(new_path, "%s", SECRET_FOLDER_PATH);
    } else {
        sprintf(new_path, "%s%s", SECRET_FOLDER_PATH, path);
    }

    DIR *dp = opendir(new_path);
    if (dp == NULL)
        return -errno;

    struct dirent *de;
    while ((de = readdir(dp)) != NULL) {
        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0) {
            continue;
        }

        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if (filler(buf, de->d_name, &st, 0))
            break;
    }

    closedir(dp);
    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    (void) fi;

    if (!is_authenticated) {
        printf("Please login first.\n");
        return -1;
    }

    char new_path[MAX_LINE_LENGTH];

    if (strcmp(path, "/") == 0) {
        sprintf(new_path, "%s", SECRET_FOLDER_PATH);
    } else {
        sprintf(new_path, "%s%s", SECRET_FOLDER_PATH, path);
    }

    int fd = open(new_path, O_RDONLY);
    if (fd == -1)
        return -errno;

    int res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);
    return res;
}

static int xmp_rename(const char *from, const char *to)
{
    if (!is_authenticated) {
        printf("Please login first.\n");
        return -1;
    }

    char src_path[MAX_LINE_LENGTH];
    char dst_path[MAX_LINE_LENGTH];

    if (strcmp(from, "/") == 0) {
        sprintf(src_path, "%s", SECRET_FOLDER_PATH);
    } else {
        sprintf(src_path, "%s%s", SECRET_FOLDER_PATH, from);
    }

    if (strcmp(to, "/") == 0) {
        sprintf(dst_path, "%s", SECRET_FOLDER_PATH);
    } else {
        sprintf(dst_path, "%s%s", SECRET_FOLDER_PATH, to);
    }

    int res = rename(src_path, dst_path);
    if (res == -1)
        return -errno;

    return 0;
}

void generateResultFile() {
    char command[MAX_LINE_LENGTH];
    sprintf(command, "tree %s > %s", SECRET_FOLDER_PATH, RESULT_FILE_PATH);
    int result = system(command);
    
    if (result != 0) {
        fprintf(stderr, "Gagal membuat file hasil\n");
        return;
    }
    
    printf("Berhasil membuat file hasil\n");
}

void generateExtensionFile() {
    char command[MAX_LINE_LENGTH];
    sprintf(command, "find %s -type f | awk -F . '{print $NF}' | sort | uniq -c | awk '{print $2 \" = \" $1}' > %s", SECRET_FOLDER_PATH, EXTENSION_FILE_PATH);
    int result = system(command);
    
    if (result != 0) {
        fprintf(stderr, "Gagal membuat file ekstensi\n");
        return;
    }
    
    printf("Berhasil membuat file ekstensi\n");
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename = xmp_rename,  
};

int main(int argc, char *argv[]) {
    umask(0);

    const char* url = "https://drive.google.com/u/0/uc?id=18YCFdG658SALaboVJUHQIqeamcfNY39a&export=download&confirm=yes";
    const char* zipFileName = "rahasia.zip";
    
    if (downloadFile(url, zipFileName) != 0) {
        fprintf(stderr, "Gagal mendownload file zip\n");
        return 1;
    }
    
    if (unzipFile(zipFileName) != 0) {
        fprintf(stderr, "Gagal melakukan unzip\n");
        return 1;
    }
    
    printf("Unzip file zip berhasil\n");

    int choice;
    while (1) {
        printf("Opsi:\n");
        printf("0. Registrasi\n");
        printf("1. Login\n");
        printf("Masukkan pilihan: ");
        if (scanf("%d", &choice) != 1) {
            printf("Input tidak valid. Silakan coba lagi.\n");
            while (getchar() != '\n'); // Clear the input buffer
            continue;
        }

        if (choice == 0) {
            User user;
            printf("\nRegistrasi User\n");
            printf("Masukkan Username: ");
            scanf("%s", user.username);
            printf("Masukkan Password: ");
            scanf("%s", user.password);
            md5hash(user.password, user.password);
            registerUser(&user);
            break;
        } else if (choice == 1) {
            User loginUserInput;
            printf("\nLogin User\n");
            printf("Masukkan Username: ");
            scanf("%s", loginUserInput.username);
            printf("Masukkan Password: ");
            scanf("%s", loginUserInput.password);
            md5hash(loginUserInput.password, loginUserInput.password);

            if (loginUser(&loginUserInput) == 0) {
                printf("Login Berhasil\n");
                is_authenticated = true;
                struct fuse_args args = FUSE_ARGS_INIT(0, NULL);
                fuse_opt_add_arg(&args, argv[0]);
                fuse_opt_add_arg(&args, MOUNTPOINT_PATH);
                fuse_opt_add_arg(&args, "-o");
                fuse_opt_add_arg(&args, "nonempty");

                generateResultFile();
                generateExtensionFile();

                return fuse_main(args.argc, args.argv, &xmp_oper, NULL);
            } else {
                printf("Username atau Password Salah\n");
                return 1;
            }
        } else {
            printf("Pilihan tidak valid. Silakan coba lagi.\n");
        }
    }
    
    return 0;
}