# sisop-praktikum-modul-4-2023-MHFD-IT06


- Fathika Afrine Azaruddin - 5027211016
- Athallah Narda Wiyoga - 5027211041
- Gilbert Immanuel Hasiholan - 5027211056

# Soal no. 1

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
```
Kode di atas merupakan inklusi dari beberapa header file standar yang diperlukan untuk fungsi-fungsi dasar dalam bahasa C, seperti stdio.h untuk fungsi input-output standar, stdlib.h untuk fungsi-fungsi umum, dan string.h untuk fungsi-fungsi pemrosesan string.

```c
#define MAX_PLAYERS 20000
#define MAX_LINE_LENGTH 1000
```
Kode ini mendefinisikan konstanta MAX_PLAYERS dan MAX_LINE_LENGTH dengan nilai tertentu. MAX_PLAYERS menentukan jumlah maksimum pemain dalam array players, dan MAX_LINE_LENGTH menentukan panjang maksimum baris yang dapat dibaca dari file CSV.

```c
struct Player {
    char name[50];
    int age;
    char club[50];
    int potential;
    char photo[100];
    char nation[50];
};
```
Kode di atas mendefinisikan sebuah struktur Player yang memiliki beberapa field untuk menyimpan informasi tentang seorang pemain sepak bola, seperti nama, usia, klub, potensi, URL foto, dan negara.

```c
int main() {
```
Fungsi main() adalah titik masuk utama program.

```c
char *api_token = getenv("KAGGLE_API_TOKEN");
if (api_token == NULL) {
    fprintf(stderr, "KAGGLE_API_TOKEN environment variable not set.\n");
    return 1;
}
```
Kode di atas mencoba mengambil nilai token dari environment variable KAGGLE_API_TOKEN menggunakan fungsi getenv(). Jika nilai token tidak ada atau tidak terdefinisi (nilai NULL), maka program akan mencetak pesan kesalahan ke stderr dan mengembalikan nilai 1 untuk menandakan kesalahan.

```c
system("kaggle datasets download -d bryanb/fifa-player-stats-database");
```
Perintah di atas menggunakan system() untuk menjalankan perintah sistem dalam shell. Ini akan mengunduh dataset yang diperlukan menggunakan kaggle API.

```c
system("unzip fifa-player-stats-database.zip");
```
Perintah di atas menggunakan system() untuk menjalankan perintah sistem untuk mengekstrak dataset yang telah diunduh sebelumnya menggunakan perintah unzip.

```c
printf("Dataset downloaded and extracted successfully.\n");
```
Kode ini mencetak pesan sukses bahwa dataset telah berhasil diunduh dan diekstrak.

```c
FILE *fp;
char line[MAX_LINE_LENGTH];
struct Player players[MAX_PLAYERS];
int num_players = 0;
```
Kode di atas mendeklarasikan file pointer fp untuk membaca file CSV, array line untuk menyimpan baris data yang dibaca, array players untuk menyimpan data pemain, dan variabel num_players untuk menghitung jumlah pemain yang telah diproses.

```c
fp = fopen("FIFA23_official_data.csv", "r");
if (fp == NULL) {
    fprintf(stderr, "Error opening file.\n");
    return 1;
}
```
Kode di atas membuka file "FIFA23_official_data.csv" dalam mode "r" (hanya untuk membaca). Jika file tidak dapat dibuka, program mencetak pesan kesalahan ke stderr dan mengembalikan nilai 1 untuk menandakan kesalahan.

```c
fgets(line, MAX_LINE_LENGTH, fp);  // Skip header line.
```
Kode di atas membaca baris header pertama dari file CSV menggunakan fgets(). Baris ini dilewati karena berisi nama kolom dan tidak mengandung data pemain yang sebenarnya.

```c
while (fgets(line, MAX_LINE_LENGTH, fp)) {
    //...
}
```
Kode di atas memulai loop untuk membaca setiap baris data dari file CSV menggunakan fgets(). Loop ini akan berjalan sampai tidak ada baris data yang tersisa.

```c
char *token;
char *rest = line;
struct Player player;
```
Di dalam loop, kode di atas mendeklarasikan variabel token dan rest untuk digunakan dalam memecah baris data menjadi token-token terpisah. Selain itu, variabel player digunakan untuk menyimpan informasi pemain dari baris data yang sedang diproses.

```c
token = strtok_r(rest, ",", &rest);  // ID
token = strtok_r(rest, ",", &rest);  // Name
strncpy(player.name, token, sizeof(player.name));
//...
```
Kode di atas menggunakan strtok_r() untuk memecah baris data menjadi token berdasarkan delimiter "," (koma). Setiap token kemudian digunakan untuk mengisi field-field yang sesuai dalam struktur Player. Misalnya, token pertama adalah ID (tidak disimpan dalam struktur Player), token kedua adalah nama, dan disalin ke field name menggunakan strncpy().

```c
if (strcmp(token, "\"") != 0) {
    //...
    players[num_players++] = player;
}
```
Setelah semua field diisi dengan token yang sesuai, kode di atas memeriksa apakah pemain memiliki posisi yang valid. Jika pemain memiliki posisi (token bukan "), maka pemain tersebut memenuhi kriteria. Informasi pemain tersebut dicetak, dan data pemain disimpan dalam array players menggunakan operator ++ untuk mengincrement num_players.

```c
fclose(fp);
```
Kode di atas menutup file yang telah dibuka menggunakan fclose().

```c
return 0;
```
Kode ini mengembalikan nilai 0 untuk menandakan bahwa program berjalan dengan sukses dan selesai.

Result :
https://ibb.co/bK1QhvG

# Soal no. 5

Setelah sukses menjadi pengusaha streaming musik di chapter kehidupan sebelumnya, Elshe direkrut oleh lembaga rahasia untuk membuat sistem rahasia yang terenkripsi.  Kalian perlu membantu Elshe dan membuat program rahasia.c. Pada program rahasia.c, terdapat beberapa hal yang harus kalian lakukan sebagai berikut.

**a**. Program rahasia.c merupakan file FUSE yang akan digunakan untuk melakukan mount folder pada Docker Container. Unduh file rahasia.zip kemudian lakukan unzip pada file rahasia.zip menjadi folder rahasia.

Untuk poin a ini, kita perlu membuat fungsi untuk mendowload file serta fungsi untuk melakukan unzipping file, seperti sebagai berikut

```c
int downloadFile(const char* url, const char* outputFileName) {
    if (fileExists(outputFileName)) {
        printf("File %s sudah ada, tidak perlu mendownload lagi\n", outputFileName);
        return 0;
    }
    
    char command[256];
    sprintf(command, "wget -O %s \"%s\"", outputFileName, url);
    
    int result = system(command);
    
    if (result != 0) {
        fprintf(stderr, "Gagal mendownload file zip\n");
        return 1;
    }
    
    return 0;
}

int unzipFile(const char* zipFileName) {
    if (directoryExists(SECRET_FOLDER_PATH)) {
        printf("Direktori rahasia sudah ada, tidak perlu melakukan unzip lagi\n");
        return 0;
    }

    char parent_directory[MAX_LINE_LENGTH];
    strcpy(parent_directory, SECRET_FOLDER_PATH);
    char *last_slash = strrchr(parent_directory, '/');
    if (last_slash != NULL) {
        *last_slash = '\0';
    }

    char command[256];
    sprintf(command, "unzip -o %s -d %s", zipFileName, parent_directory);
    
    int result = system(command);
    
    if (result != 0) {
        fprintf(stderr, "Gagal melakukan unzip\n");
        return 1;
    }
    
    return 0;
}
```

Untuk main functionnya seperti berikut:
```c
int main(int argc, char *argv[]) {
    umask(0);

    const char* url = "https://drive.google.com/u/0/uc?id=18YCFdG658SALaboVJUHQIqeamcfNY39a&export=download&confirm=yes";
    const char* zipFileName = "rahasia.zip";
    
    if (downloadFile(url, zipFileName) != 0) {
        fprintf(stderr, "Gagal mendownload file zip\n");
        return 1;
    }
    
    if (unzipFile(zipFileName) != 0) {
        fprintf(stderr, "Gagal melakukan unzip\n");
        return 1;
    }
    
    printf("Unzip file zip berhasil\n");
}
```

Hasil outputnya:
![check](https://cdn.discordapp.com/attachments/818011685695520769/1114388904334868510/image.png)

**b**. Seperti soal a, folder rahasia akan di-mount pada Docker Container dengan image bernama rahasia_di_docker_Kode Kelompok pada direktori /usr/share. Gunakan Ubuntu Focal Fossa sebagai base image pada Dockerfile.

Untuk pengerjaan poin b ini karena dari IT sendiri tidak dipelajari, maka kita tidak menggunakan docker, sehingga jadinya saya mount folder rahasia ke dalam direktori di local seperti berikut

![check](https://cdn.discordapp.com/attachments/818011685695520769/1114382033653874698/image.png)

**c**. Setelah melakukan mount, buatlah register system yang menyimpan kredensial berupa username dan password. Agar lebih aman, password disimpan dengan menggunakan hashing MD5. Untuk mempermudah kalian, gunakan system()  serta gunakan built-in program untuk melakukan hashing MD5 pada Linux (tidak wajib). Username dan password akan disimpan dengan format username;password. Tidak boleh ada user yang melakukan register dengan username yang sama. Kemudian, Buatlah login system yang mencocokkan kredensial antara username dan password.

Untuk poin c, kita perlu membuat sistem register dan login, dimana ketika sudah register maka username dan password yang dihash akan disimpan ke dalam credentials.txt, dan jika salah log in maka tidak bisa melakukan mount.

Berikut merupakan fungsi yang digunakan:
```c
void registerUser(const User* user) {
    FILE* file = fopen(CREDENTIALS_FILE, "a+");
    if (!file) {
        fprintf(stderr, "Gagal membuka file kredensial\n");
        return;
    }

    char line[MAX_LINE_LENGTH];
    while (fgets(line, MAX_LINE_LENGTH, file)) {
        User existingUser;
        sscanf(line, "%[^;];%s\n", existingUser.username, existingUser.password);

        if (strcmp(user->username, existingUser.username) == 0) {
            fprintf(stderr, "Username sudah ada\n");
            fclose(file);
            return;
        }
    }

    fprintf(file, "%s;%s\n", user->username, user->password);
    fclose(file);
}

int loginUser(const User* user) {
    FILE* file = fopen(CREDENTIALS_FILE, "r");
    if (!file) {
        fprintf(stderr, "Gagal membuka file kredensial\n");
        return 1;
    }

    char line[MAX_LINE_LENGTH];
    while (fgets(line, MAX_LINE_LENGTH, file)) {
        User existingUser;
        sscanf(line, "%[^;];%s\n", existingUser.username, existingUser.password);

        if (strcmp(user->username, existingUser.username) == 0 && strcmp(user->password, existingUser.password) == 0) {
            fclose(file);
            return 0;
        }
    }

    fclose(file);
    return 1;
}
```

Untuk main functionnya seperti berikut:
```c
int choice;
    while (1) {
        printf("Opsi:\n");
        printf("0. Registrasi\n");
        printf("1. Login\n");
        printf("Masukkan pilihan: ");
        if (scanf("%d", &choice) != 1) {
            printf("Input tidak valid. Silakan coba lagi.\n");
            while (getchar() != '\n'); // Clear the input buffer
            continue;
        }

        if (choice == 0) {
            User user;
            printf("\nRegistrasi User\n");
            printf("Masukkan Username: ");
            scanf("%s", user.username);
            printf("Masukkan Password: ");
            scanf("%s", user.password);
            md5hash(user.password, user.password);
            registerUser(&user);
            break;
        } else if (choice == 1) {
            User loginUserInput;
            printf("\nLogin User\n");
            printf("Masukkan Username: ");
            scanf("%s", loginUserInput.username);
            printf("Masukkan Password: ");
            scanf("%s", loginUserInput.password);
            md5hash(loginUserInput.password, loginUserInput.password);

            if (loginUser(&loginUserInput) == 0) {
                printf("Login Berhasil\n");
            }
        }
    }
```
Berikut merupakan outputnya:

![check](https://cdn.discordapp.com/attachments/818011685695520769/1114385339168079932/image.png)

![check](https://cdn.discordapp.com/attachments/818011685695520769/1114385675647721512/image.png)

**d**. Folder tersebut hanya dapat diakses oleh user yang telah melakukan login. User yang login dapat membaca folder dan file yang di-mount kemudian dengan menggunakan FUSE lakukan rename pada folder menjadi Nama_Folder_Kode_Kelompok dan Kode_Kelompok_Nama_File.ext.

Untuk poin d, kita akan melakukan rename pada folder rahasia melalui direktori mount yang sudah kita buat, untuk itu kita menggunakan fungsi FUSE seperti ini:
```c
static int xmp_rename(const char *from, const char *to)
{
    if (!is_authenticated) {
        printf("Please login first.\n");
        return -1;
    }

    char src_path[MAX_LINE_LENGTH];
    char dst_path[MAX_LINE_LENGTH];

    if (strcmp(from, "/") == 0) {
        sprintf(src_path, "%s", SECRET_FOLDER_PATH);
    } else {
        sprintf(src_path, "%s%s", SECRET_FOLDER_PATH, from);
    }

    if (strcmp(to, "/") == 0) {
        sprintf(dst_path, "%s", SECRET_FOLDER_PATH);
    } else {
        sprintf(dst_path, "%s%s", SECRET_FOLDER_PATH, to);
    }

    int res = rename(src_path, dst_path);
    if (res == -1)
        return -errno;

    return 0;
}
```

**e**. List seluruh folder, subfolder, dan file yang telah di-rename dalam file result.txt menggunakan tree kemudian hitung file tersebut berdasarkan extension dan output-kan menjadi extension.txt.

Untuk poin terakhir ini, kita membuat fungsi yang akan membuat dua file yakni result.txt dan extension.txt, dimana result.txt berisi tree dari folder rahasia dan extension.txt yang menghitung extension apa saja yang ada di folder rahasia.

Berikut merupakan fungsi yang digunakan:
```c
void generateResultFile() {
    char command[MAX_LINE_LENGTH];
    sprintf(command, "tree %s > %s", SECRET_FOLDER_PATH, RESULT_FILE_PATH);
    int result = system(command);
    
    if (result != 0) {
        fprintf(stderr, "Gagal membuat file hasil\n");
        return;
    }
    
    printf("Berhasil membuat file hasil\n");
}

void generateExtensionFile() {
    char command[MAX_LINE_LENGTH];
    sprintf(command, "find %s -type f | awk -F . '{print $NF}' | sort | uniq -c | awk '{print $2 \" = \" $1}' > %s", SECRET_FOLDER_PATH, EXTENSION_FILE_PATH);
    int result = system(command);
    
    if (result != 0) {
        fprintf(stderr, "Gagal membuat file ekstensi\n");
        return;
    }
    
    printf("Berhasil membuat file ekstensi\n");
}
```
result.txt:

![check](https://cdn.discordapp.com/attachments/818011685695520769/1114387923937276016/image.png)

extension.txt:

![check](https://cdn.discordapp.com/attachments/818011685695520769/1114388330377908234/image.png)
