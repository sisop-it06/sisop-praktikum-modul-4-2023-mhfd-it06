#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 20000

int main() {
    FILE *fp;
    char line[MAX_LEN];

    fp = fopen("FIFA23_official_data.csv", "r");

    if (fp == NULL) {
        printf("Cannot open file.\n");
        exit(1);
    }

    // skip the header line
    fgets(line, MAX_LEN, fp);

    while (fgets(line, MAX_LEN, fp)) {
        char *token;
        char name[MAX_LEN], nationality[MAX_LEN], club[MAX_LEN], photo_url[MAX_LEN];
        int age, potential;
        
        // get player info from each line
        token = strtok(line, ",");
        int i = 0;
        while (token != NULL) {
            switch (i) {
                case 1: strcpy(name, token); break;
                case 2: age = atoi(token); break;
                case 3: strcpy(photo_url, token); break;
                case 4: strcpy(nationality, token); break;
                case 7: potential = atoi(token); break;
                case 8: strcpy(club, token); break;
            }
            i++;
            token = strtok(NULL, ",");
        }

        // print player info if they meet the criteria
        if (age < 25 && potential > 85 && strcmp(club, "Manchester City") != 0) {
            printf("Name: %s\n", name);
            printf("Age: %d\n", age);
            printf("Potential: %d\n", potential);
            printf("Club: %s\n", club);
            printf("Nationality: %s\n", nationality);
            printf("Photo URL: %s\n", photo_url);
            printf("\n");
        }
    }

    fclose(fp);
    return 0;
}
